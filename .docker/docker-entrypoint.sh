#!/usr/bin/env bash

set -e

CMD=$(which server)
PORT="${PORT:-8100}"
DB_HOST="${DB_HOST:-elnaz_shop_db}"
DB_PORT="${DB_PORT:-5432}"
DB_NAME="${DB_NAME:-elnaz}"
DB_USER="${DB_USER:-elnaz}"
DB_PASS="${DB_PASS:-elnaz}"

if [[ "$1" == "" ]]; then
    exec "${CMD}" -port="${PORT}"      \
                  -dbhost="${DB_HOST}" \
                  -dbport="${DB_PORT}" \
                  -dbname="${DB_NAME}" \
                  -dbuser="${DB_USER}" \
                  -dbpass="${DB_PASS}"
else
    exec "$@"
fi