package main

import (
	"log"
	"os"

	"github.com/gin-gonic/gin"
)

func init() {
	if os.Getenv("RUNMODE") == "production" {
		gin.SetMode(gin.ReleaseMode)
	}
}

func main() {
	r := gin.New()
	r.Use(gin.Logger(), gin.Recovery())
	migrate()
	makeRoutes(r)
	log.Fatal(r.Run(":" + os.Getenv("PORT")))
}
