package main

import (
	"github.com/gin-gonic/gin"
)

func makeRoutes(r *gin.Engine) {
	product_api := r.Group("/product")
	buildProductRoutes(product_api)
}

func buildProductRoutes(r *gin.RouterGroup) {
	r.POST("/", SaveProduct)
	r.GET("/", GetProducts)
	r.GET("/:id", GetProduct)
	r.POST("/delete", DeleteProduct)
}
