package main

import (
	"encoding/json"
	"fmt"

	"github.com/go-playground/validator/v10"
)

func validationErrors(err error) string {
	invalid := make(map[string]string)
	if errs, ok := err.(validator.ValidationErrors); ok {
		for _, err := range errs {
			invalid[err.Field()] = err.Tag()
		}
		json_invalid, _ := json.Marshal(invalid)
		return string(json_invalid[:])
	} else {
		return fmt.Sprintf("%v", err)
	}
}
