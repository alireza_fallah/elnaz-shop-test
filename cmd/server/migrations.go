package main

import (
	"log"

	"gitlab.com/alireza_fallah/elnaz-shop-test/models"
	"gorm.io/gorm"
)

func migrate() {

	db_models := []interface{}{
		&models.Product{},
	}
	// models.DB.Migrator().DropTable(&models.Product{})//
	if err := models.DB.AutoMigrate(db_models...); err != nil {
		log.Fatal(err)
	}
	for _, model := range db_models {
		DropUnusedColumns(model)
	}
}
func DropUnusedColumns(dst interface{}) {
	stmt := &gorm.Statement{DB: models.DB}
	stmt.Parse(dst)
	fields := stmt.Schema.Fields
	columns, _ := models.DB.Debug().Migrator().ColumnTypes(dst)
	for i := range columns {
		found := false
		for j := range fields {
			if columns[i].Name() == fields[j].DBName {
				found = true
				break
			}
		}
		if !found {
			models.DB.Migrator().DropColumn(dst, columns[i].Name())
		}
	}
}
