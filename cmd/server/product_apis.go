package main

import (
	"log"
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/alireza_fallah/elnaz-shop-test/models"
)

func SaveProduct(c *gin.Context) {
	params := &models.ProductParams{}
	if err := c.ShouldBindJSON(params); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": validationErrors(err)})
		return
	}
	p := &models.Product{}
	if params.ID > 0 {
		p.ID = params.ID
		if err := p.FindByID(); err != nil {
			c.JSON(http.StatusNotFound, gin.H{"error": "Product not found"})
			return
		}
	}
	if err := p.Save(params); err != nil {
		log.Print("SERVER ERROR: ", err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Server error"})
		return
	}

	c.JSON(http.StatusOK, p)
	return
}

func GetProducts(c *gin.Context) {
	p := &models.Product{}
	//limit >>>
	var err error
	limit := 0
	limit_param, _ := c.GetQuery("limit")
	if limit_param != "" {
		limit, err = strconv.Atoi(limit_param)
		if err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Limit must be integer"})
			return
		}
	}
	////
	products, err := p.FindAll(limit)
	if err != nil {
		log.Print("SERVER ERROR: ", err.Error())
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Server error"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"count": len(products), "products": products})
	return
}

func GetProduct(c *gin.Context) {
	id_param := c.Param("id")
	id, err := strconv.Atoi(id_param)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "ID must be integer"})
		return
	}
	p := &models.Product{ID: id}
	if err := p.FindByID(); err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Product not found"})
		return
	}
	c.JSON(http.StatusOK, p)
	return
}

type RemoveProductParams struct {
	ID int `json:"id" binding:"required"`
}

func DeleteProduct(c *gin.Context) {
	params := &RemoveProductParams{}
	if err := c.ShouldBindJSON(params); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": validationErrors(err)})
		return
	}
	p := &models.Product{}
	p.ID = params.ID
	if err := p.FindByID(); err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Product not found"})
		return
	}
	if err := p.Remove(); err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Server error"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"result": "success"})
}
