package models

import (
	"flag"
	"fmt"
	"log"
	"os"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

var (
	port   = flag.String("port", os.Getenv("PORT"), "http server port")
	dbHost = flag.String("dbhost", os.Getenv("DB_HOST"), "database hostname")
	dbPort = flag.String("dbport", os.Getenv("DB_PORT"), "database port")
	dbUser = flag.String("dbuser", os.Getenv("DB_USER"), "database user")
	dbPass = flag.String("dbpass", os.Getenv("DB_PASS"), "database password")
	dbName = flag.String("dbname", os.Getenv("DB_NAME"), "database name")
)
var DB *gorm.DB

const STATUS_DISABLED = 0
const STATUS_ENABLED = 1

func init() {
	flag.Parse()
	dsn := fmt.Sprintf(
		"host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		*dbHost, *dbPort, *dbUser, *dbPass, *dbName,
	)
	database, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
		DisableForeignKeyConstraintWhenMigrating: true,
	})
	if err != nil {
		log.Fatal(err)
	}
	DB = database
}
func GetDB() *gorm.DB {
	return DB
}
