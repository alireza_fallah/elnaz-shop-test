package models

import (
	"time"

	"gorm.io/gorm"
)

type Product struct {
	ID        int       `gorm:"primaryKey;autoIncerement" json:"id"`
	Name      string    `gorm:"not null;index" json:"name"`
	Status    int       `gorm:"not null;index" json:"status"`
	CreatedAt time.Time `gorm:"index" json:"created_at"`
}

//data input from user
type ProductParams struct {
	ID     int    `json:"id"`
	Name   string `json:"name" binding:"required"`
	Status int    `json:"status" binding:"required"`
}

func (p *Product) Save(params *ProductParams) error {
	//if ID exists update
	if params.ID > 0 {
		p.ID = params.ID
		if err := p.FindByID(); err != nil {
			return err
		}
		p.Name = params.Name
		p.Status = params.Status
		return DB.Save(p).Error
	}
	p.Name = params.Name
	p.Status = params.Status
	return DB.Save(p).Error
}
func (p *Product) BeforeCreate(tx *gorm.DB) (err error) {
	p.CreatedAt = time.Now()
	return
}

//id must be present
func (p *Product) FindByID() error {
	return DB.Model(p).Where("id=?", p.ID).First(p).Error
}

func (p *Product) FindAll(limit int) ([]*Product, error) {
	if limit == 0 {
		limit = 100
	}
	var products []*Product
	if err := DB.Model(p).Where("status=?", STATUS_ENABLED).Order("id desc").Limit(limit).Find(&products).Error; err != nil {
		return nil, err
	}
	return products, nil
}

//id must be present
func (p *Product) Remove() error {
	if err := p.FindByID(); err != nil {
		return err
	}
	return DB.Where("id =?", p.ID).Delete(p).Error
}
